This project creates a repository & front-end for:

  Searching EULA, terms & conditions, privacy, etc. that end-users sign up
  from different software vendors and service providers

The ElasticSearch repository is hosted using the **sandbox tier** of
[facetflow](facetflow.com) which supports:

1. 5,000 documents
1. 500 MB storage
1. 1 primary shard
1. 0 replicas

h2. ES Document Types & Strcucture

TODO


